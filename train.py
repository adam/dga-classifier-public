#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import sys

from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix

from joblib import dump

f_data = sys.argv[1]
f_model = sys.argv[2]

ts=0.2

col_X = 'label'
col_Y = 'c'

print("Reading dataset {} ...".format(f_data))
df_in = pd.read_csv(f_data)

print("\t{} rows".format(df_in.shape[0]))

class_names, class_counts = np.unique(df_in[col_Y], return_counts=True)

for i in range(len(class_names)):
	print("\tclass: {} = {}".format(class_names[i], class_counts[i]))

print()
print("Splitting dataset (test_size={}) ...".format(ts))
X_train, X_test, Y_train, Y_test = train_test_split(df_in[col_X].values.astype('U'), df_in[col_Y], test_size=ts)


print("\ttrain = {}".format(X_train.shape[0]))
print("\ttest = {}".format(X_test.shape[0]))

dga_classifier = Pipeline([
	('cntvect', CountVectorizer(ngram_range=(2,2),analyzer='char')),
	('clf', RandomForestClassifier(n_estimators=200, random_state=0),)
])

print()
print("Fitting classifier ...")
dga_classifier = dga_classifier.fit(X_train, Y_train)

print("\taccuracy =  {}".format(dga_classifier.score(X_test, Y_test)))

preds = dga_classifier.predict(X_test)

print("\nConfusion matrix")
print(confusion_matrix(Y_test, preds))

print("\nClassification report")
print(classification_report(Y_test, preds))

print("Saving model to {} ...".format(f_model))
dump(dga_classifier, f_model, compress=3)

print("Done.")

# DGA classifier

A model for DGA domains detection. Based on character-level bigrams in a domain name label.

## Requirements

Python 3.7.6 with packages:

```
joblib==0.14.1
scikit-learn==0.22.1
numpy==1.18.1
pandas==1.0.3
```

## DGA detection

This repository contains a [pre-trained model](models/model1) which was trained using 100k labels of real .CZ domains and known DGA domains. In order to classify labels use [predict.py](predict.py) script.

`./predict.py <model> <input> <output>`

where:

  * `<model>` - a model saved in a Python [joblib](https://joblib.readthedocs.io/) dump
  * `<input>` - input file (CSV with header, must contain `label` column. Labels must not have TLD suffix, for example, to get a prediction for `ceskatelevize.cz` a label `ceskatelevize` should be provided)
  * `<output>` - output file (CSV with header, contains the following columns: `label`, `predicted` - predicted class for a label (**DGA** or **legit**), `prob` - prediction probability)

For example:
```
$ ./predict.py models/model1 data/some_labels.csv /tmp/some_labels_predicted.csv
Reading model models/model1 ...
Reading dataset data/some_labels.csv ...
	12 rows
Classifying ...
	class: dga = 6
	class: legit = 6
Saving dataset /tmp/some_labels_predicted.csv ...
Done.
```
```
$ cat /tmp/some_labels_predicted.csv
label,predicted,prob
nic,legit,1.0
root,legit,0.995
seznam,legit,1.0
mapy,legit,0.985
ceskatelevize,legit,1.0
idnes,legit,0.99
wiqrty,dga,0.87
fgdsag,dga,0.665
88fadfsdqr3,dga,0.85
p0d7fv,dga,0.835
fdf35fc,dga,0.705
abcdefghi,dga,0.74
```

## Training a custom model

In order to train your own model use [train.py](train.py) script:

`./train.py <input> <model>`

where:

  * `<input>` - input file (CSV with header, must contain `label` and `c` (actual class for a given `label`) columns. **REMARK:** Labels must not have TLD suffix, for example: use `ceskatelevize` label instead of FQDN `ceskatelevize.cz`)
  * `<model>` - a file to save model in a Python [joblib](https://joblib.readthedocs.io/) dump

```
$ ./train.py data/labels-100k.csv models/test1 
Reading dataset data/labels-100k.csv ...
	100000 rows
	class: dga = 47910
	class: legit = 52090

Splitting dataset (test_size=0.25) ...
	train = 75000
	test = 25000

Fitting classifier ...
	accuracy =  0.96472

Confusion matrix
[[11213   759]
 [  123 12905]]

Classification report
              precision    recall  f1-score   support

         dga       0.99      0.94      0.96     11972
       legit       0.94      0.99      0.97     13028

    accuracy                           0.96     25000
   macro avg       0.97      0.96      0.96     25000
weighted avg       0.97      0.96      0.96     25000

Saving model to models/test1 ...
Done.
```
More classifier option can be set directly in [train.py](train.py) code.

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import sys

from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier

from joblib import load

f_model = sys.argv[1]
f_data_in = sys.argv[2]
f_data_out = sys.argv[3]

col_X = 'label'
col_Y = 'predicted'

print("Reading model {} ...".format(f_model))
dga_classifier = load(f_model)

print("Reading dataset {} ...".format(f_data_in))
df_in = pd.read_csv(f_data_in)

print("\t{} rows".format(df_in.shape[0]))

print("Classifying ...".format(f_data_in))

preds_proba = dga_classifier.predict_proba(df_in[col_X].values.astype('U'))

df_out = pd.DataFrame.from_dict({'label': df_in[col_X],  col_Y: dga_classifier.classes_.take(np.argmax(preds_proba, axis=1), axis=0), 'prob': np.max(preds_proba, axis=1)})

class_names, class_counts = np.unique(df_out[col_Y], return_counts=True)
for i in range(len(class_names)):
	print("\tclass: {} = {}".format(class_names[i], class_counts[i]))

print("Saving dataset {} ...".format(f_data_out))
df_out.to_csv(f_data_out, index = False)

print("Done.")
